from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.views import LoginView, UserModel
from django.contrib.auth import authenticate, login, logout
from django.views.generic import RedirectView
from django.http import HttpResponse, JsonResponse, request

#Django-Sesame libs
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from sesame import tokens
from sesame.utils import get_user, get_query_string, get_token

from user.models import User
from login.forms import LoginForm
from django.http import HttpResponseRedirect

def loginView(request):
    if request.user.is_authenticated:
            print(request.user)
            return redirect('/home')
    form = LoginForm(request.POST or None)
    if request.POST:       
        if form.is_valid():
            user = form.login(request)
            if user:
                login(request, user)
                return HttpResponseRedirect("/home")
        else:
            print("form no valid")
    else:
        form=LoginForm()
    return render(request, 'inicio2.html', {'form':form})

class LogoutRedirectView(RedirectView):
    pattern_name='index'

    def dispatch(self, request, *args, **kwargs):
        logout(request)
        return super().dispatch(request, *args, **kwargs)

def loginView2(request):
    if request.user.is_authenticated:
            print(request.user)
            return redirect('/home')
    form = LoginForm(request.POST or None)
    if request.POST:       
        if form.is_valid():
            user = form.login(request)
            if user:
                login(request, user)
                return HttpResponseRedirect("/home")
        else:
            print("form no valid")
    else:
        form=LoginForm()
    return render(request, 'inicio2.html', {'form':form})

