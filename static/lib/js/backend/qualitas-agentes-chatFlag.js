'use-strict'
const ASW = require('aws-sdk');
const documentClient = new ASW.DynamoDB.DocumentClient();


exports.handler = async (event, context) => {
    let responseBody = "";
    let statusCode = 0;
   

try {
        const params = {
            TableName: "chatSwitch",
         };
        var allData=new Array;
        await getAllData(params, allData);
        responseBody = JSON.stringify(allData);
         statusCode = 200;
    } catch(error) {
        console.log(error);
        responseBody = error;
        statusCode = 403;
    }
    
    
    const response = {
        statusCode: statusCode,
        body: responseBody,
        headers: {'Content-Type': 'application/json',
             "Access-Control-Allow-Origin": "*"
        }
    };
    return response;
};



const getAllData = async (params, allData) => { 
    const data = await documentClient.scan(params).promise();
    if(data.Items){
       data.Items.forEach(function(item) {
          allData.push(item); 
       });
    }
        return data;
}

