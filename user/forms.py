from django.forms import *
from user.models import User

class UserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['autofocus'] = True

    class Meta:
        model = User
        fields = '__all__'
        widgets = {
            'username': TextInput(
                attrs={
                    'placeholder': 'Usuario',
                }
            ),
            'first_name': TextInput(
                attrs={
                    'placeholder': 'Nombre(s)',
                }
            ),
            'last_name': TextInput(
                attrs={
                    'placeholder': 'Apellido',
                }
            ),
            'email': TextInput(
                attrs={
                    'placeholder': 'Email',
                }
            ),
            'code': TextInput(
                attrs={
                    'placeholder': 'Código',
                }
            ),
            'region': TextInput(
                attrs={
                    'placeholder': 'Region',
                }
            ),
            'office': TextInput(
                attrs={
                    'placeholder': 'Oficina',
                }
            ),            'verified': TextInput(
                attrs={
                    'placeholder': 'Código',
                    'value':'0',
                }
            ),
            'password':PasswordInput(
                attrs={
                    'placeholder': 'Password',
                }
            ),
        }
        exclude=['last_login','is_superuser','is_staff','is_active','date_joined']

        def save(self, commit=True):
            data={}
            form=super()
            try: 
                if form.is_valid():
                    form.save()
                else:
                    data['error']=form.errors
            except Exception as e:
                data['error']=str(e)
            return data

